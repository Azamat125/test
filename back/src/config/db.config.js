const mysql = require("mysql2");

const connection = mysql.createConnection({
  host: `mysqldb`,
  user: `dbuser`,
  password: `dbuserpwd`,
  database: `boilerplate_db`,
  // socketPath: "/var/lib/mysql/mysql.sock",
});

module.exports = connection;
