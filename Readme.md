<h1>Инструкция для работы без докера</h1>
<ul>
    <li>
        Заходим в консоль MYSQL(предварительно загрузить, если нет БД MYSQL То переходим по ссылку для загрузки дистрибутива 
        <strong style="color:blue">https://dev.mysql.com/downloads/mysql/</strong>  
        Я использовал root пользователя и пароль  для подключения к БД
    </li>
    <li>
        Вводим команду <br> <strong style="color:orange">source c:/users/aza/desktop/testBackendRus/firstCommand.sql</strong>  <br>
        В вашем случае путь к файлу будет другой
        В случае ошибок можно поискать решения по данной ссылке 
        <br>
        <strong style="color:blue">https://stackoverflow.com/questions/8940230/how-to-run-sql-script-in-mysql</strong>  
    </li>
    <li>
        Далее проверяем есть ли Node.js.
        Я использую GitBash. Ввожу команду <br> <strong style="color:orange">node -v</strong>  <br>И выводится номер версии
         <br>
         Если не установлен Node.js, то устанавливаем по ссылке
          <strong style="color:blue">https://nodejs.org/en/</strong>  
    </li>
    <li>
        Далее находясь в родительском каталоге вводим команду 
        <br> <strong style="color:orange">npm i</strong>  <br>
        Дожидаемся установки всех зависимостей
    </li>
     <li>
       Дождавшись установки, вводим команду
        <br> <strong style="color:orange">npm run dev</strong>  <br>
    </li>
    <li>
       Далее открываем в браузере <br> <strong style="color:orange">Index.html</strong>  <br>
    </li>
     <li>
        Для тестирования http запросов можно использовать postman по адресу 
        <br> <strong style="color:orange"> для сохранения пользователя Post Запрос http://localhost:3001/users</strong>  <br>
         <br> <strong style="color:orange"> для получения токена Post Запрос http://localhost:3001/users/session</strong>  <br>
    </li>
</ul>

<h1>Инструкция для работы с докера</h1>
<ul>
    <li>
        В терминале запускам  
        <br> <strong style="color:orange">docker-compose up</strong>  <br>
    </li>
    <li>
        если в терминале видим ошибку 
        <br> <strong style="color:orange">   connect ECONNREFUSED 172.26.0.2:3306</strong>  <br>
    </li>
    <li>
        то в файле db.config необходимо убрать кооментарии с поля socketPath  
        и далее повторяем с 1 шага
        <br> <strong style="color:orange">какая-та магия либо с докером на винде либо с образом mysql или с либой mysql2</strong>  <br>
    </li>

</ul>
